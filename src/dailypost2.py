#%%
import csv
import numpy as np
import requests
from bs4 import BeautifulSoup

# 

#%%

def getLinks( pagesCount = 5):
    listLinks = []
    for i in range(40,pagesCount):
        mainUrl = "https://dailypost.ng/hot-news/page/" + str(i) + '/'
        source = requests.get(mainUrl).text
        soup  = BeautifulSoup(source,'lxml')
        pageLinks = soup.findAll('li', {'class':['mvp-blog-story-wrap', 'left']})
        for attachLink in pageLinks:
            listLinks.append(attachLink.a['href'])
    return listLinks

#%%
#<h1 class="mvp-post-title left entry-title" itemprop="headline"></h1>
def linkToText(link):
    source = requests.get(link).text
    s = BeautifulSoup(source,'lxml')
    info1 = s.find('h1', {'class':['mvp-post-title']}).text
    info2 = [l.text for l in  s.find_all('p')][9:40]
    info2 = ''.join(str(e) for e in info2)
    info3 = s.find('time',class_ ='post-date').text
    info4 = s.find('span',class_ ='author-name').a.text
    return [info4,info3,info1, info2]
   

#%%
def main():
    csvfile = open('new_jenu.csv', 'w') 
    csvWriter = csv.writer(csvfile)
    csvWriter.writerow(['Author','Date','Title','Text'])
    count = 0
    links = getLinks(pagesCount=80)
    print("Web Crawler Started")
    for singleLink in links:
        count+=1
        listText = linkToText(singleLink)
        author = listText[0]
        date = listText[1]
        title = listText[2]
        text = listText[3]
        csvWriter.writerow([author,date,title,text])
        print(count)
    print("Web Crawler 100% completed")
  



main()




#%%
