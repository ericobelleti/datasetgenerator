#%%

from bs4 import BeautifulSoup
import csv
import requests

#%%
baseUrl="https://www.vanguardngr.com/tag/boko-haram/page/"
mainUrl = baseUrl + str(1)
source = requests.get(mainUrl).text
source
#%%

soup  = BeautifulSoup(source,'lxml')
pageLinks = soup.find_all('a')


#%%
pageLinks

#%%

def getLinks(baseUrl = "https://www.vanguardngr.com/tag/boko-haram/page/", pagesCount = 130):
    listLinks = []
    for i in range(pagesCount):
        mainUrl = baseUrl + str(i)
        source = requests.get(mainUrl).text
        soup  = BeautifulSoup(source,'lxml')
        pageLinks = soup.find_all('a', class_ = 'rtp-read-more-link')
        for attachLink in pageLinks:
            listLinks.append(attachLink.get('href'))

    return listLinks


def linkToText(link):
    source = requests.get(link).text
    s = BeautifulSoup(source,'lxml')
    try:
       info1 = s.find('h1').text
       info2 = [l.text for l in  s.find_all('p')][9:40]
       info2 = ''.join(str(e) for e in info2)
       info3 = s.find('time',class_ ='entry-date').text
       info4 = s.find('span',class_ ='byline2')
       return [info4.text,info3,info1, info2]
    except:
         return ['','','','']

    




def main():
    csvfile = open('vangr.csv', 'w') 
    csvWriter = csv.writer(csvfile)
    csvWriter.writerow(['Author','Date','Title','Text'])
    count = 0
    links = getLinks()
    print("Web Crawler Started")
    for singleLink in links:
        count+=1
        listText = linkToText(singleLink)
        author = listText[0]
        date = listText[1]
        title = listText[2]
        text = listText[3]
        csvWriter.writerow([author,date,title,text])
        print(count)
    print("Web Crawler 100% completed")

main()
